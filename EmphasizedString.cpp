/*
* Copyright 2019  Son Geon kde.jen6@gmail.com
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "EmphasizedString.h"
#include <iostream>

namespace AST{
  EmphasizeString::EmphasizeString() {
  }

  int EmphasizeString::clearNonComplete(std::vector<Emphasize>::iterator nCompIt) {
    int bufTokenLen = 0;
    int dis = std::distance(empSt.begin(), nCompIt);

    for(auto it=empSt.cbegin() + dis; 
        it != empSt.cend(); ++it) {
      const auto &elem = *it;
      Piece pc;
      pc.start = 0;
      pc.len = 2;
      pc.bufIdx = elem.bufIdx;
      pieceMap.insert(
          std::make_pair(elem.start-1, pc));
      bufTokenLen += 2;
    }
    addedTokenLen += bufTokenLen;
    empSt.erase(nCompIt, empSt.end());
    return bufTokenLen;
  }

  void EmphasizeString::addEmphToken(EmphasizeType empt){
    for(auto it = empSt.begin(); it != empSt.end(); ++it){
      //if pair found
      if(it->empType == empt) {
        it->start += addedTokenLen + 1;
        it->end = currPos + addedTokenLen -1;
        it->end += clearNonComplete(it+1);

        emps.emplace_back(*it);
        empSt.pop_back();
        return;
      }
    }

    switch(empt) {
      case EmphasizeType::BOLD:
        buffer.emplace_back("**");
        break;
      case EmphasizeType::CANCLE:
        buffer.emplace_back("~~");
        break;
      case EmphasizeType::UNDERLINE:
        buffer.emplace_back("++");
        break;
      case EmphasizeType::ITALIC:
        buffer.emplace_back("__");
        break;
      default:
        return;
    }

    Emphasize emp;
    emp.start = currPos-1;
    emp.bufIdx = buffer.size() - 1;
    emp.empType = empt;
    empSt.emplace_back(emp);
  }

  void EmphasizeString::appendText(const std::string& text){
    if(buffer.size() == 0) {
      buffer.emplace_back("");
    }

    Piece pc;
    pc.start = buffer[0].length(); 
    pc.len = text.length();
    pc.bufIdx = 0;
    
    buffer[0] += text;
    pieceMap.insert(std::make_pair(currPos ,pc));

    currPos += text.length();
  }

  std::string EmphasizeString::getString(){
    std::string ret = "";
    clearNonComplete(empSt.begin());
    for(const auto& iter : pieceMap) {
      const auto& emp = iter.second;
      const auto& text =  buffer[emp.bufIdx];
      ret += text.substr(emp.start, emp.len);
    }
    return ret;
  }

  const std::vector<Emphasize> 
    &EmphasizeString::getEmphasizes() const {
    return emps;
  }
};
