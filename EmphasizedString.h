/*
* Copyright 2019  Son Geon kde.jen6@gmail.com
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef EMPHASIZESTRING_
#define EMPHASIZESTRING_
#include "boost/range/iterator_range.hpp"

#include <map>
#include <vector>
#include <string>

namespace AST {
  enum class EmphasizeType {
    DEFAULT = 0,
    BOLD,
    CANCLE,
    UNDERLINE,
    ITALIC
  };

  struct Piece {
    int start = 0, len = 0;
    int bufIdx = 0;
  };

  struct Emphasize {
    int start = 0, end = 0, bufIdx = 0;
    EmphasizeType empType = EmphasizeType::DEFAULT;
  };

  using PieceMap = std::multimap<int, Piece>;
  using PieceMapIter = PieceMap::iterator;
  using contentIterRange = boost::range_iterator<std::string::iterator>;

  class EmphasizeString {
    private:
      int currPos = 0, addedTokenLen = 0;
      std::vector<std::string> buffer;
      std::vector<Emphasize>  empSt, emps;
      PieceMap pieceMap;

    private:
      int clearNonComplete(std::vector<Emphasize>::iterator );
    public:
      EmphasizeString();
      void addEmphToken(EmphasizeType empt);
      void appendText(const std::string& text);
      std::string getString();
      const std::vector<Emphasize> &getEmphasizes() const;
  };
};
#endif
