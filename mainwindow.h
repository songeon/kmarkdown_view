#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "textstylefactory.h"
#include "parser.h"

#include <QMainWindow>
#include <QFile>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QTextDocument>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
private slots:
    void open();
private:
    void createMenuBar();
    void createTextDocument();

    void loadFile(const QString&);
    void render();
    void renderEmphasize(QTextCursor &, const AST::String &);
    QMenu *pMenubar = nullptr;
    QFile *file = nullptr;
    QVBoxLayout *layout = nullptr;
    QTextEdit *ptextEdit;
    QTextDocument *pdoc;
    TextStyleFactory *tsFactory;

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
