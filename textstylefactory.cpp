#include "textstylefactory.h"

TextStyleFactory::TextStyleFactory()
{

}

QTextCharFormat TextStyleFactory::getByFlag(unsigned int flag) {
  QTextCharFormat form;
  if (flag & TextStyle::Normal) {
    form.setFontFamily("Courier");
    form.setFontPointSize(normalFontSize);
    form.setFontWeight(QFont::Normal);
    form.setFontUnderline(false);
    form.setFontItalic(false);
  }

  if (flag & TextStyle::Italic) {
    form.setFontItalic(true);
  }
  if (flag & TextStyle::Bold) {
    form.setFontWeight(QFont::Bold);
  }
  if (flag & TextStyle::Underline) {
    form.setFontUnderline(true);
  }

  if (flag == TextStyle::H1) {
    form.setFontPointSize(HeaderFontSize[1]);
    form.setFontWeight(QFont::Bold);
    form.setFontUnderline(true);
  } else if (flag == TextStyle::H2) {
    form.setFontPointSize(HeaderFontSize[2]);
    form.setFontWeight(QFont::Bold);
  } else if (flag == TextStyle::H3) {
    form.setFontPointSize(HeaderFontSize[3]);
    form.setFontWeight(QFont::Bold);
  } else if (flag == TextStyle::H4) {
    form.setFontPointSize(HeaderFontSize[4]);
    form.setFontWeight(QFont::Bold);
  } else if (flag == TextStyle::H5) {
    form.setFontPointSize(HeaderFontSize[5]);
    form.setFontWeight(QFont::Bold);
  } else if (flag == TextStyle::H6) {
    form.setFontPointSize(HeaderFontSize[6]);
    form.setFontWeight(QFont::Bold);
  }

  return form;
}
