#ifndef KMARKDOWNPARSER 
#define KMARKDOWNPARSER

#include "EmphasizedString.h"

#include <QString>
#include "boost/optional/optional.hpp"
#include "boost/variant/variant.hpp"

using boost::optional;

namespace AST {
  using boost::variant;
  using StrIter = std::string::iterator;
  const int CMAX_EMPHS_SIZE = 10;
  
  struct Header { 
    unsigned long level = 0; 
  };
  struct BlockQuote {};
  using Attribute = variant<Header, BlockQuote>;

  struct String {
    std::string content = "";
    std::vector<Emphasize> emphs;
    std::vector<Attribute> attr;
  };
}
AST::String ParseLine(const QString& qstr);

#endif
