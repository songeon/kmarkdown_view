#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "boost/variant/get.hpp"
#include <QFileDialog>
#include <QTextStream>
#include <QScrollBar>

#include <iostream>

namespace boost
{
  void throw_exception(std::exception const &) {}
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    layout = new QVBoxLayout(this);
    this->setLayout(layout);
    createMenuBar();
    createTextDocument();
    setCentralWidget(ptextEdit);

    tsFactory = new TextStyleFactory();
    QTextCursor cursor(pdoc);

    cursor.insertText(tr("H1"), tsFactory->getByFlag(TextStyle::H1));
    cursor.insertBlock();
    cursor.insertText(tr("H2"), tsFactory->getByFlag(TextStyle::H2));
    cursor.insertBlock();
    cursor.insertText(tr("H3"), tsFactory->getByFlag(TextStyle::H3));
    cursor.insertBlock();
    cursor.insertText(tr("H4"), tsFactory->getByFlag(TextStyle::H4));
    cursor.insertBlock();
    cursor.insertText(tr("H5"), tsFactory->getByFlag(TextStyle::H5));
    cursor.insertBlock();
    cursor.insertText(tr("H6"), tsFactory->getByFlag(TextStyle::H6));
    cursor.insertBlock();
    unsigned int boldStyle = TextStyle::Normal | TextStyle::Italic;
    cursor.insertText(tr("googler"), tsFactory->getByFlag(boldStyle));

    ptextEdit->show();

}

void MainWindow::createTextDocument() {
    ptextEdit = new QTextEdit(this);
    ptextEdit->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    ptextEdit->setAcceptRichText(true);
    ptextEdit->verticalScrollBar()->minimum();

    QPalette p = ptextEdit->palette();
    p.setColor(QPalette::Base, Qt::white);
    p.setColor(QPalette::Text, Qt::black);
    this->setPalette(p);

    ptextEdit->setTextColor(Qt::black);
    ptextEdit->resize(this->maximumSize());
    ptextEdit->setText("front");

    pdoc = new QTextDocument(ptextEdit);
    ptextEdit->setDocument(pdoc);
    layout->addWidget(ptextEdit);
}

void MainWindow::createMenuBar() {
    QMenu * pMenu = menuBar()->addMenu(tr("Open"));

    QAction *openAction = new QAction(tr("Open File"));
    openAction->setStatusTip("open file");
    openAction->setShortcut(tr("Ctrl+O"));
    connect(openAction,  &QAction::triggered,
            this, &MainWindow::open);

    pMenu->addAction(openAction);


}

void MainWindow::open()
{
    QString fileName = QFileDialog::getOpenFileName(this);
    if (!fileName.isEmpty())
            loadFile(fileName);
}

void MainWindow::loadFile(const QString &fname) {
   if(file != nullptr) {
       if(file->isOpen()) {
           file->close();
           file = nullptr;
       }
   }

   file = new QFile(fname);
   if(!file->open(QFile::ReadOnly | QFile::Text)) {
       exit(EXIT_FAILURE);
   }
   render();
}

void MainWindow::render() {
  pdoc->clear();
  const auto normal = tsFactory->getByFlag(TextStyle::Normal);

  QTextCursor cursor(pdoc);
  QTextStream stream(file);
  while(!stream.atEnd()) {
    auto style = normal;
    QString line = stream.readLine();
    AST::String ps = ParseLine(line);
    if(ps.attr.size() > 0) {
      int level = boost::get<AST::Header>(ps.attr[0]).level;
      switch(level) {
        case 1:
          style = tsFactory->getByFlag(TextStyle::H1);
          break;
        case 2:
          style = tsFactory->getByFlag(TextStyle::H2);
          break;
        case 3:
          style = tsFactory->getByFlag(TextStyle::H3);
          break;
        case 4:
          style = tsFactory->getByFlag(TextStyle::H4);
          break;
        case 5:
          style = tsFactory->getByFlag(TextStyle::H5);
          break;
        case 6:
          style = tsFactory->getByFlag(TextStyle::H6);
          break;
      }

    } 
    cursor.insertText(QString::fromStdString(ps.content), style);
    renderEmphasize(cursor, ps);
    cursor.insertBlock();
  }
}

void MainWindow::renderEmphasize(QTextCursor& cursor, 
                            const AST::String &strParsed) {
  using namespace AST;
  cursor.movePosition(QTextCursor::StartOfLine, QTextCursor::MoveAnchor); 
  int lineStart = cursor.position();
  std::cout << "line start : " << lineStart << "  emph len : " << strParsed.emphs.size() << std::endl;
  for(const auto &emph : strParsed.emphs) {
    QTextCharFormat modif;
    switch (emph.empType) {
      case EmphasizeType::BOLD:
        std::cout << "bold\n";
        modif = tsFactory->getByFlag(TextStyle::Bold);
        break;
      case EmphasizeType::CANCLE:
        std::cout << "calcle\n";
        modif = tsFactory->getByFlag(TextStyle::Cancelline);
        break;
      case EmphasizeType::UNDERLINE:
        std::cout << "underline\n";
        modif = tsFactory->getByFlag(TextStyle::Underline);
        break;
      case EmphasizeType::ITALIC:
        std::cout << "italic\n";
        modif = tsFactory->getByFlag(TextStyle::Italic);
        break;
    }

    cursor.setPosition(lineStart + emph.start, QTextCursor::MoveAnchor);
    cursor.setPosition(lineStart + emph.end+1, QTextCursor::KeepAnchor);
    std::cout << cursor.selectedText().toUtf8().constData() << std::endl;
    cursor.mergeCharFormat(modif);
  }
  cursor.movePosition(QTextCursor::EndOfLine, QTextCursor::MoveAnchor);
}


MainWindow::~MainWindow()
{
    delete ui;
}
