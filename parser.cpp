/*
* Copyright 2019  Son Geon kde.jen6@gmail.com
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "parser.h"

#include "boost/spirit/home/x3.hpp"
#include "boost/spirit/include/qi.hpp"
#include "boost/fusion/include/adapt_struct.hpp"
#include "boost/phoenix/phoenix.hpp"
#include "boost/range/iterator_range.hpp"

#include <string>
#include <iostream>
#include <tuple>
#include <typeinfo>
#include <stack>
#define BOOST_SPIRIT_X3_DEBUG

using namespace boost::spirit;
using namespace boost::phoenix;
using boost::optional;

BOOST_FUSION_ADAPT_STRUCT(AST::String, content)
BOOST_FUSION_ADAPT_STRUCT(AST::Header, level)
BOOST_FUSION_ADAPT_STRUCT(AST::Emphasize, start)
BOOST_FUSION_ADAPT_STRUCT(AST::BlockQuote)

namespace parser
{
  namespace phx = boost::phoenix;
  namespace x3 = boost::spirit::x3;
  namespace ascii = boost::spirit::x3::ascii;
  namespace fus = boost::fusion;

  using x3::_val;
  using x3::_attr;
  using x3::int_;
  using x3::lit;
  using x3::double_;
  using x3::lexeme;
  using x3::repeat;
  using ascii::char_;
  using x3::omit;
  using x3::raw;

  using StringType = x3::rule<class String_, AST::String>;
  using HeaderType = x3::rule<class Header_, AST::Header>;
  using OnceType = x3::rule<class Once_>;
  using ContentType = x3::rule<class Content_, std::string>;
  using EmphasizeType = x3::rule<class EmphasizeType_, AST::EmphasizeType>;
  using EmphasizeStringType = x3::rule<
    class EmphasizeString_, AST::EmphasizeString>;
  BOOST_SPIRIT_DECLARE(StringType, HeaderType, OnceType, 
      ContentType, EmphasizeType, EmphasizeStringType)
  
  StringType const String = "String";
  HeaderType const Header = "Header";
  OnceType  const   Once = "Once";
  ContentType const Content = "Content";
  EmphasizeType const Emphasize = "Emphasize";
  EmphasizeStringType const EmphasizeString = "EmphasizeString";

  const auto Print = [](auto& ctx) {
    std::cout << _attr(ctx).level << std::endl;
  };

  const auto Bold   = lit("**");
  const auto Cancle = lit("~~");
  const auto Underline = lit("++");
  const auto Italic = lit("__");
  const auto EmphasizeTokens = (Bold | Cancle | Underline | Italic);

  const auto GenerateEmphasize = [](AST::EmphasizeType et) {
    return [et](auto& ctx) {
      _val(ctx) = et;
    };
  };

  const auto Emphasize_def = 
    Bold[ GenerateEmphasize(AST::EmphasizeType::BOLD) ]
    | Cancle[ GenerateEmphasize(AST::EmphasizeType::CANCLE)]
    | Underline[ GenerateEmphasize(AST::EmphasizeType::UNDERLINE)]
    | Italic[ GenerateEmphasize(AST::EmphasizeType::ITALIC)];
  BOOST_SPIRIT_DEFINE(Emphasize);

  auto const Content_def =  +(char_ - EmphasizeTokens);
  BOOST_SPIRIT_DEFINE(Content);

  const auto setEmpText = [](auto& ctx) {
    _val(ctx).appendText(_attr(ctx));
  };

  const auto setEmp = [](auto& ctx) {
    _val(ctx).addEmphToken(_attr(ctx));
  };

  const auto EmphasizeString_def = 
    +(omit[Emphasize[setEmp]] | Content[setEmpText]);
  BOOST_SPIRIT_DEFINE(EmphasizeString);

  auto HeaderLevel = [](auto& ctx) { 
    _val(ctx) = AST::Header{ _attr(ctx).size() };
  };

  auto const Header_def = 
    repeat(1, 6)[char_('#')][HeaderLevel] >> ' ';
  BOOST_SPIRIT_DEFINE(Header);
    
  const auto pushFunc = [](auto& ctx) {
    _val(ctx).attr.push_back(_attr(ctx));
  };

  const auto getEmphStr = [](auto& ctx) { 
    _val(ctx).content += _attr(ctx).getString(); 
    _val(ctx).emphs = _attr(ctx).getEmphasizes();
  };

  const auto setContent = [](auto& ctx) {
    _val(ctx).content += _attr(ctx);
  };

  auto const String_def= 
    omit[Header[pushFunc]] >> Content[setContent]
    | EmphasizeString[getEmphStr];
  BOOST_SPIRIT_DEFINE(String);
}

AST::String ParseLine(const QString& qstr) {
  using boost::spirit::x3::ascii::space;
  using boost::spirit::x3::phrase_parse;
  using boost::spirit::x3::parse;

  std::string str = qstr.toUtf8().constData();
  AST::String m = AST::String{};
  auto r =  parse(str.begin(), str.end(),
      parser::String, m);

  return m;
}

//int main() {
//  std::string str;
//  std::getline(std::cin, str);
//  
//  using boost::spirit::x3::ascii::space;
//  using boost::spirit::x3::phrase_parse;
//  using boost::spirit::x3::parse;
//
//  AST::String m = AST::String{};
//  auto r =  parse(str.begin(), str.end(),
//      parser::String, m);
//  if(!r) return 0;
//
//  if(m.attr.size() > 0)
//    std::cout << boost::get<AST::Header>(m.attr[0]).level;
//  std::cout << "\nContent : " << m.content << "\n";
//}
